<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\UserForm;
use Illuminate\Http\Request;

class UserFormController extends Controller
{
    public function index()
    {
        return view('backend.index');
    }
    public function register()
    {
        return view('backend.register');
    }
   /* public function register_store(Request $request)
    {
        $user = new UserForm();
        $user->fullName = $request->input('fullName');
        $user->email = $request->input('email');
        $user->usernames = $request->input('usernames');
        $user->password = bcrypt($request->input('password')); // Hash the password

        // Save the user to the database
        $user->save();

        // Redirect to a success page or login page
        return redirect()->route('backend.login')->with('success', 'Registration successful!');
    }
    */
    public function register_store(Request $request)
    {
        $data=$request->all();
        UserForm::create($data);
        return redirect()->route('backend.login')->with('success', 'inserted successful!');
    }

    public function showLoginForm()
    {
        return view('backend.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Perform your custom logic to check the user's credentials here
        $email = $request->input('email');
        $password = $request->input('password');

        // Replace the following example with your actual user validation logic
        if ($this->customUserValidation($email, $password)) {
            // Custom validation passed, you can redirect to the intended page
            return redirect()->route('backend.index');
        }

        // Custom validation failed
        return back()->withErrors(['email' => 'Invalid credentials']);
    }

    // Implement your custom user validation logic here
    private function customUserValidation($email, $password)
    {
        // Replace this example with your actual logic, e.g., querying the database
        $validUser = DB::table('user_forms')
            ->where('email', $email)
            ->where('password', $password)
            ->exists();

        return $validUser;
    }

}