<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function product_store(Request $request)
    {
        $data=$request->all();
        Product::create($data);
        return redirect()->route('backend.product')->with('success', 'inserted successful!');
    }
    public function product()
{
    $data = Product::all();
    return view('backend.product', compact('data')); // Pass the $data variable to the view
}
    public function table()
    {
        $data = Product::all();
    }
    public function product_edit($id)
    {
        // Fetch the product by ID
        $product = Product::find($id);

        // Check if the product exist
        // Pass the product data to the edit view
        return view('backend.product_edit', compact('product'));
    }
    public function update(Request $request , $id)
    {
        $product= $request->except('_token');
        Product::Where('id',$id)->update($product);
        
        return redirect()->route('backend.product')->with('success', 'Updated successful!');
    }
}
