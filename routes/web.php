<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserFormController;
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('backend.login');
});
Route::get('/index', [UserFormController::class, 'index'])->name('backend.index');
Route::get('/register', [UserFormController::class, 'register'])->name('backend.register');
Route::post('/register_store', [UserFormController::class, 'register_store'])->name('backend.register_store');
Route::get('/login', [UserFormController::class, 'showLoginForm'])->name('backend.login'); // Rename 'login' method
Route::post('/login', [UserFormController::class, 'login'])->name('backend.login_submit'); // Add route for login form submission
Route::get('/product', [ProductController::class, 'product'])->name('backend.product');
Route::post('/product_store', [ProductController::class, 'product_store'])->name('backend.product_store');
Route::post('/product', [ProductController::class, 'table']);
Route::get('/product_edit/{id}', [ProductController::class, 'product_edit'])->name('backend.product_edit');
Route::post('/update/{id}', [ProductController::class, 'update'])->name('backend.product_update');

