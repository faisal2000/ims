<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('barcode')->notnullable();
            $table->string('pname')->notnullable();
            $table->string('pprice')->notnullable();
            $table->string('sprice')->notnullable();
            $table->string('profit')->notnullable();
            $table->string('stock')->notnullable();
            $table->string('rdate')->notnullable();
            $table->string('cname')->notnullable();
            $table->string('sname')->notnullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
