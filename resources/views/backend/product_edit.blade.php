@extends('backend.master')

@section('content')
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product ADD</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
                        <form Action="Action="{{ route('backend.product_update', $product->id) }}"  method="get">
                            @csrf
                            <!-- Barcode Input -->
                            <div class="form-group">
                                <label for="barcode">Barcode</label>
                                <input type="text" name="barcode" class="form-control" value="{{$product->barcode}}" id="barcode" placeholder="Enter Barcode">
                            </div>

                            <!-- Product Name Input -->
                            <div class="form-group">
                                <label for="productName">Product Name</label>
                                <input type="text"name="pname" class="form-control" value="{{$product->pname}}" id="productName" placeholder="Enter Product Name">
                            </div>

                            <!-- Purchase Price Input -->
                            <div class="form-group">
                                <label for="purchasePrice">Purchase Price</label>
                                <input type="number" name="pprice"class="form-control" value="{{$product->pprice}}" id="purchasePrice" placeholder="Enter Purchase Price" oninput="calculateProfit()">
                            </div>

                            <!-- Selling Price Input -->
                            <div class="form-group">
                                <label for="sellingPrice">Selling Price</label>
                                <input type="number"name="sprice" class="form-control" value="{{$product->sprice}}" id="sellingPrice" placeholder="Enter Selling Price" oninput="calculateProfit()">
                            </div>

                            <!-- Profit Input -->
                            <div class="form-group">
                                <label for="profit">Profit</label>
                                <input type="number" name="profit"class="form-control" value="{{$product->profit}}" id="profit" placeholder="Enter Profit" readonly>
                            </div>

                            <div class="form-group">
                                <label for="stock">Stock</label>
                                <input type="number" name="stock"class="form-control" value="{{$product->stock}}" id="profit" placeholder="Stock" readable>
                            </div>

                            <!-- Received Date Input -->
                            <div class="form-group">
                                <label for="receivedDate">Received Date</label>
                                <input type="text" name="rdate"class="form-control" value="{{$product->rdate}}" id="receivedDate">
                            </div>

                            <!-- Company Name Input -->
                            <div class="form-group">
                                <label for="companyName">Company Name</label>
                                <input type="text"name="cname" class="form-control" value="{{$product->cname}}" id="companyName" placeholder="Enter Company Name">
                            </div>

                            <!-- Supplier Name Input -->
                            <div class="form-group">
                                <label for="supplierName">Supplier Name</label>
                                <input type="text"name="sname" class="form-control" value="{{$product->sname}}" id="supplierName" placeholder="Enter Supplier Name">
                            </div>
                            
                                
                                <button type="submit" class="btn btn-primary mt-5">Save changes</button>
                        </form>
    <script>
        function calculateProfit() {
            // Get the values of purchase and selling prices
            var purchasePrice = parseFloat(document.getElementById("purchasePrice").value);
            var sellingPrice = parseFloat(document.getElementById("sellingPrice").value);

            // Calculate profit (selling price - purchase price)
            var profit = sellingPrice - purchasePrice;

            // Update the profit input field
            document.getElementById("profit").value = profit;
        }
    </script>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>

@endsection
