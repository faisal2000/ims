<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <title>Login Page</title>
    <style>
        body {
            background-color: #3498db;
        }
        .center-div {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
        .form-control {
            width: 300px;
        }
        .card {
            width: 400px;
            text-align: center;
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
            margin:200px;
        }
        .card-header {
            text-align: center;
            color: yellowgreen;
            background-color: #f0f0f0;
            font-weight: bold;
            padding: 10px;
            border-radius: 5px 5px 0 0;
        }
        .login-btn {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .form-label{
            font-size:20px;
            color:green;
        }
    </style>
</head>
<body>
    <div class="center-div">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Login</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('backend.login_submit') }}" method="post"> <!-- Use the named route 'login.submit' -->
                        @csrf
                        <div class="form-group">
                            <label for="username">Email</label>
                            <input type="email" name="email" id="username" class="form-control" placeholder="Enter your username" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                    <p>Don't Have an Account <a href="{{route('backend.register')}}" ><b>Register</b></a></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
