@extends('backend.master')

@section('content')
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product ADD</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <style>
        #mbtn{
            margin-top:-70px;
            margin-left:980px;
            height:50px;
        }
        .search{
            margin-left:10px;
            width:500px;
            height:50px;
        }
        #sbtn{
            height:50px;

        }
        .card-header{
            font-weight: bold;
            color:black;
            text-align: center;
            font-size:20px;
    }
    .card-body{
        font-size:12px;
        font-weight: bold;
        
    }
    .no-product{
        font-size:20px;
        text-align: center;

    }
    .sbtn{
        color:white;
        background-color:#0d6efd;
        border-color: #0d6efd;
        
    }
    .mbtn{
        color:white;
        background-color:#dc3545;
        border-color: #dc3545;
        
    }
    .dbtn{
        color:white;
        background-color:#ffc720;
        border-color: #ffc720;

    }
    </style>
    <div class="container">
        <form action="/search" method="GET">
            <input type="text" name="q" class="search mt-5" placeholder="Search...">
            <button  id="sbtn" class="btn btn-primary" type="submit">Search</button>
        </form>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary " id="mbtn" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <i class='bx bx-plus-medical'></i> Add Product
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                        <form Action="{{route('backend.product_store')}}" method="post">
                            @csrf
                            <!-- Barcode Input -->
                            <div class="form-group">
                                <label for="barcode">Barcode</label>
                                <input type="text" name="barcode" class="form-control" id="barcode" placeholder="Enter Barcode">
                            </div>

                            <!-- Product Name Input -->
                            <div class="form-group">
                                <label for="productName">Product Name</label>
                                <input type="text"name="pname" class="form-control" id="productName" placeholder="Enter Product Name">
                            </div>

                            <!-- Purchase Price Input -->
                            <div class="form-group">
                                <label for="purchasePrice">Purchase Price</label>
                                <input type="number" name="pprice"class="form-control" id="purchasePrice" placeholder="Enter Purchase Price" oninput="calculateProfit()">
                            </div>

                            <!-- Selling Price Input -->
                            <div class="form-group">
                                <label for="sellingPrice">Selling Price</label>
                                <input type="number"name="sprice" class="form-control" id="sellingPrice" placeholder="Enter Selling Price" oninput="calculateProfit()">
                            </div>

                            <!-- Profit Input -->
                            <div class="form-group">
                                <label for="profit">Profit</label>
                                <input type="number" name="profit"class="form-control" id="profit" placeholder="Enter Profit" readable>
                            </div>

                            <div class="form-group">
                                <label for="stock">Stock</label>
                                <input type="number" name="stock"class="form-control" id="profit" placeholder="Stock" readable>
                            </div>

                            <!-- Received Date Input -->
                            <div class="form-group">
                                <label for="receivedDate">Received Date</label>
                                <input type="date" name="rdate"class="form-control" id="receivedDate">
                            </div>

                            <!-- Company Name Input -->
                            <div class="form-group">
                                <label for="companyName">Company Name</label>
                                <input type="text"name="cname" class="form-control" id="companyName" placeholder="Enter Company Name">
                            </div>

                            <!-- Supplier Name Input -->
                            <div class="form-group">
                                <label for="supplierName">Supplier Name</label>
                                <input type="text"name="sname" class="form-control" id="supplierName" placeholder="Enter Supplier Name">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
            </div> 
            </div>
        </div>
        </div>

            <div class="card">
                <div class="card-body">
                    <div class="card-header shadow ">
                        Product List
                    </div>
                    <table id="productTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Serial No.</th>
                                <th>Barcode</th>
                                <th>Product Name</th>
                                <th>purchase Price</th>
                                <th>Selling Price</th>
                                <th>Profit</th>
                                <th>Stock</th>
                                <th>Received Date</th>
                                <th>Company Name</th>
                                <th>Supplier Name</th>
                                <!-- Add more table headers as needed -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->barcode }}</td>
                                <td>{{ $user->pname }}</td>
                                <td>{{ $user->pprice }}</td>
                                <td>{{ $user->sprice }}</td>
                                <td>{{ $user-> profit}}</td>
                                <td>{{ $user->stock }}</td>
                                <td>{{ $user->rdate }}</td>
                                <td>{{ $user->cname }}</td>
                                <td>{{ $user-> sname}}</td>
            
                               
                                <td>
                                    
                                   <button class="sbtn ">Show</button>
                                   <a href="{{route('backend.product_edit',['id' => $user->id]) }}" ><button class="mbtn">Edit</button></a> 
                                    <button class="dbtn">Delete</button>
                                </td>
                            </tr>
                            @endforeach
                           
                        </tbody>
                    </table>

                </div>
            </div>
    </div>

    <script>
    $(document).ready(function () {
        // Select the search input and table
        var $searchInput = $('.search');
        var $table = $('#productTable');
        var $tbody = $table.find('tbody');
        var $noProductRow = $('<tr class="no-product"><td colspan="11">No product found</td></tr>');

        // Add an event listener to the search input
        $searchInput.on('keyup', function () {
            var searchText = $(this).val().toLowerCase();

            // Hide the "No product found" row initially
            $noProductRow.hide();

            // Iterate through each row of the table
            $table.find('tbody tr').each(function () {
                var $row = $(this);
                var rowData = $row.text().toLowerCase();

                // Show or hide the row based on the search text
                if (rowData.indexOf(searchText) === -1) {
                    $row.hide();
                } else {
                    $row.show();
                    $row.addClass('highlight');
                }
            });

            // Show "No product found" if no rows are visible
            if ($table.find('tbody tr:visible').length === 0) {
                $noProductRow.show();
                $tbody.append($noProductRow);
            } else {
                $noProductRow.hide();
            }
        });

        // Initialize DataTables
        var dataTable = $table.DataTable({
            "paging": true,
            "pageLength": 10,
            "lengthMenu": [10, 25, 50, 100],
            "ordering": true
        });
    });
    </script>

    <script>
        function calculateProfit() {
            // Get the values of purchase and selling prices
            var purchasePrice = parseFloat(document.getElementById("purchasePrice").value);
            var sellingPrice = parseFloat(document.getElementById("sellingPrice").value);

            // Calculate profit (selling price - purchase price)
            var profit = sellingPrice - purchasePrice;

            // Update the profit input field
            document.getElementById("profit").value = profit;
        }
    </script>






    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>

@endsection
